
use actix_web::{get, post, web, App, HttpResponse, HttpServer, Responder, Result, HttpRequest};
use serde::{Deserialize, Serialize};
use serde_json::json;
use serde_json::Value as json_value;


use colored::*;

use std::vec::Vec;
use serde_json::Value;
use serde_json::json as json_value;

mod jwt_lib;
use jwt_lib::{JWT, DataJWT, JWTData, Token, create_token, create_jwt, show_data};


mod jwk_lib;
use jwk_lib::{DataOriginJWK, SharedDataJWK, Keys, JwkData};

use std::collections::HashMap;


use chrono::{DateTime, Utc};

use std::sync::Mutex;

mod render_html;
use render_html::{Html, build_context, SharedDataHtmlRender};

mod certificate;
use certificate::{X509Params};

use colored::*;

mod encrypt_request;

#[derive(Serialize, Deserialize)]
struct Url {
    name: String,
}


#[derive(Serialize, Deserialize, Debug)]
struct DataOrigin {
    appid: Option<String>,
    journey_id: Option<String>,
    journey_version: Option<String>,
    data: Option<Value>,
    jwt: Option<JWTData>,
    token: Option<Token>,
    html: Option<String>,
    render_html: Option<Html>
}

fn advert_jouneys(data: &DataOrigin)  {
    let appid = data.appid.as_ref();
    let journey_id = data.journey_id.as_ref();
    let journey_version = &data.journey_version.as_ref();
    println!("{}", "¡¡¡¡REMEMBER!!!!".on_bright_red().bold());


    match appid{
        Some(value) => println!("{}{}", "Application: ".yellow(), &value.bright_yellow().bold().italic()),
        None => println!("{} no info", "Application: ".yellow())
    }

    match journey_id{
        Some(value) => println!("{}{}", "Journey id: ".green(), &value.bright_green().bold().italic()),
        None => println!("{} no info", "journey id: ".green())
    }

    match journey_version{
        Some(value) => println!("{}{}", "Journey version: ".blue(), &value.bright_blue().bold().italic()),
        None => println!("{} no info", "Journey version: ".blue())
    }
        //println!("¡¡¡¡REMEBER!!!!\r\n{}", result);


}

pub struct SharedDataJson {
    pub shared_data: Mutex<HashMap<String, Value>>,
}

struct SharedData {
    shared_data: Mutex<HashMap<String, String>>
}

fn generate_response(message: &str, is_succesful: bool) -> Result<HttpResponse> {
    let result = json!(
        {"is_succesful": is_succesful,
        "message": &message}
    );
    Ok(HttpResponse::Ok().json(result))
}

#[post("/jwt")]
async fn validate_jwt(req_body: web::Json<DataJWT>) -> Result<HttpResponse> {
    
    let jwt = &req_body.jwt;
    let jwt = JWT::new(jwt.to_string());
    //println!("{:?}",&jwt.header);
    println!("Received: \r\n\r\n");
    let jwt_str = serde_json::to_string_pretty(&jwt);
    println!("{}",jwt_str.unwrap());
    Ok(HttpResponse::Ok()
    .json(json!(jwt)))
}

#[post("/check_pin")]
async fn check_pin(req_body: web::Json<encrypt_request::ManagePin>) -> Result<HttpResponse> {
    let value = &req_body.jwt;
    let jwt_pin = JWT::validate(value, encrypt_request::PUB_KEY_TRANSMIT.to_string());
    
    if jwt_pin.validation.unwrap().is_verified == Some(true) {
        let result = encrypt_request::check_pin(&req_body.type_id, &req_body.id, &req_body.field,
            &jwt_pin.payload.as_ref().unwrap()["pin"].as_str().unwrap(), &req_body.token, &req_body.host);
        let value_pin = &result["result"][0][&req_body.field].as_str();
        match value_pin {
            Some(v) => {
                let pin_str = format!("[{}]", v);
                let pin_vec: Vec<u8> = serde_json::from_str(&pin_str).unwrap();
                let pin = encrypt_request::decrypt_public(&pin_vec, encrypt_request::PUB_KEY_LDAP);
                let is_succesful = &pin == &jwt_pin.payload.unwrap()["pin"].as_str().unwrap();
                let message = if is_succesful == true {
                    "Correct Pin" } 
                    else {
                    "Incorrect Pin"
                };
                generate_response(&message, is_succesful)
            },
            None => generate_response("Pin not match", false)
        }
    } else {
        generate_response("JWT not valid", false)
    }
}

#[post("/set_pin")]
async fn set_pin(msg: web::Json<encrypt_request::ManagePin>) -> Result<HttpResponse> {
    let value = &msg.jwt;
    let jwt_pin = JWT::validate(value, encrypt_request::PUB_KEY_TRANSMIT.to_string());

    if jwt_pin.validation.unwrap().is_verified == Some(true) {
        let pin = &jwt_pin.payload.as_ref().unwrap()["pin"].as_str().unwrap().to_owned();
        let pin_encrypted = encrypt_request::encrypt_private(&pin, encrypt_request::PK_LDAP);
        let new_pin = &jwt_pin.payload.unwrap()["new_pin"].as_str().unwrap().to_owned();
        let new_pin_encrypted = encrypt_request::encrypt_private(&new_pin, encrypt_request::PK_LDAP);
        let result = encrypt_request::assign_pin(&msg.type_id, &msg.id, &msg.field, &pin_encrypted, &new_pin_encrypted, &msg.token, &msg.host);
        let message = if result == false {
            "Change of Pin wrong" } else {
            "Change of Pin correct wrong"
        };
        let is_succesful = if result == false {
            false
        } else {
            true
        };
        Ok(HttpResponse::Ok().json(json!({"result": result, "message": message, "is_succesful": is_succesful})))
    } else {
        Ok(HttpResponse::Ok().json(json!({"message": "JWT not valid", "is_succesful": false})))
    }
}

#[get("/generate_numeric_pin/{pin}")]
async fn generate_numeric_pin(obj: web::Path<Url>) -> Result<HttpResponse> {
    let pin_str = format!("{}", obj.name.to_string());
    Ok(HttpResponse::Ok().json(json!(encrypt_request::encrypt_private(&pin_str, encrypt_request::PK_LDAP))))

}

#[post("/decrypt_numeric_pin")]
async fn decrypt_numeric_pin(msg: web::Json<encrypt_request::PIN>) -> Result<HttpResponse> {
    let pin = format!("[{}]", msg.pin.to_owned());
    let pin: Vec<u8> = serde_json::from_str(&pin).unwrap();
    println!("{:?}", &pin);
    Ok(HttpResponse::Ok().json(json!(encrypt_request::decrypt(&pin, encrypt_request::PK_TRANSMIT))))
}


#[post("/generate_private_certificate")]
async fn generate_certificate(msg: web::Json<X509Params>) -> Result<HttpResponse> {
    let version = msg.version.to_owned();
    let subject_name =  msg.subject_name.to_owned();
    let issuer_name = msg.issuer_name.to_owned();
    let not_before = msg.not_before;
    let not_after = msg.not_after;
    let num = msg.num;
    
    let cert = X509Params {
        version: version,
        subject_name: subject_name,
        issuer_name: issuer_name,
        not_before: not_before,
        not_after: not_after,
        num: num
    };

    let result = cert.generate_certificate();

    Ok(HttpResponse::Ok().json(json!({"certificate": result})))

}

#[post("/show/{uid}/{id}")]
async fn generate_msg(req: HttpRequest,  msg: web::Json<DataOrigin>, shared: web::Data<SharedData>,
                shared_html: web::Data<SharedDataHtmlRender>, shared_json: web::Data<SharedDataJson>)
                -> Result<HttpResponse> {

    println!("#######################################BEGIN##############################");
    let uid: String = req.match_info().query("uid").parse().unwrap();
    let id: String = req.match_info().query("id").parse().unwrap();
    let now: DateTime<Utc> = Utc::now();
    println!("TIME: {}", now.format("%T %e-%b-%Y"));
                    let url =
    if msg.render_html.is_none() == false {
        let render_html = msg.render_html.as_ref();
        let my_html = build_context(&render_html);
        let shared_data_html: &SharedDataHtmlRender = &shared_html;
        shared_data_html.shared_data.lock().unwrap().insert(render_html.unwrap().url.to_owned(), my_html.to_owned());
        (None, Some(format!("http://localhost/html/{}", render_html.unwrap().url)))
    } else {
        (Some("No data to render".to_string()),None)

    };
    
    let html = msg.html.as_ref();
    let shared_data: &SharedData = &shared;
    let html_info = match html {
        Some(value) => {
            println!("\r\n{} http://localhost:8000/view_html/{}", "url:".green(), &uid);
            value},
        None => "NO INFO"
    };
    shared_data.shared_data.lock().unwrap().insert(uid.to_string(), html_info.to_string());
    
    
    let status_code = show_data(&msg.data, &id, &uid);
    if status_code == 200 {
        shared_json.shared_data.lock().unwrap().insert(format!("data_{}", uid.to_owned()), msg.data.as_ref().unwrap().to_owned())}
    else {
        shared_json.shared_data.lock().unwrap().insert(format!("data_{}", uid.to_owned()), json_value!(null))
    };

    
    let jwt: JWT = create_jwt(&msg.token, &id);
    let token = create_token(&msg.jwt);
    
    println!("\r\nFrom user {} \r\nRESULTADO {:?}", &uid.blue().bold(), jwt);
    println!("For token {:?}\r\n\r\n", token);
    advert_jouneys(&msg);
    println!("#######################################END##############################");
    
    Ok(HttpResponse::Ok()
    .json(
        json!(
            {
                "url": {
                    "value": url.1,
                    "eror": url.0
                },
                "status_code": status_code,
                "jwt": jwt,
                "token": {
                    "value": token.1,
                    "error": token.0
                }
            })
        )
    )
}

#[get("/view_html/{uid>}")]
async fn index(req: HttpRequest, shared: web::Data<SharedData>) -> Result<HttpResponse> {
    let uid: String = req.match_info().query("uid").parse().unwrap();
    let pre_values = &shared.shared_data.lock().unwrap();
    let values = pre_values.get(&uid);
    let html_content = match values {
        Some(value) => value.to_owned(), 
        None => "NO INFO".to_string()
         };
    
    Ok(HttpResponse::Ok().body(html_content))
}

#[get("/html/<url>")]
async fn index_html(req: HttpRequest, shared: web::Data<SharedDataHtmlRender>) -> Result<HttpResponse> {
    let url: String = req.match_info().query("url").parse().unwrap();
    let pre_values = &shared.shared_data.lock().unwrap();
    let values = pre_values.get(&url);
    let html_content = match values {
       Some(value) => value.to_owned(), 
       None => "NO INFO".to_string()
        };
    
    Ok(HttpResponse::Ok().body(html_content))
}


#[actix_web::main]
async fn main() -> std::io::Result<()> {
    let shared_data = web::Data::new(SharedData{
        shared_data: Mutex::new(HashMap::new())
    });
    let shared_data_jwk = web::Data::new(SharedDataJWK{
        shared_data: Mutex::new(HashMap::new())
    });
    let shared_data_html_render = web::Data::new(SharedDataHtmlRender{
        shared_data: Mutex::new(HashMap::new())
    });
    let shared_data_json = web::Data::new(SharedDataJson{
        shared_data: Mutex::new(HashMap::new())
    });

    HttpServer::new(move || {
        App::new()
            .service(validate_jwt)
            .service(check_pin)
            .service(set_pin)
            .service(generate_numeric_pin)
            .service(decrypt_numeric_pin)
            .service(generate_certificate)
            .service(generate_msg)
            .service(index)
            .service(index_html)
            .app_data(shared_data.clone())
            .app_data(shared_data_jwk.clone())
            .app_data(shared_data_html_render.clone())
            .app_data(shared_data_json.clone())
        })
        .bind("0.0.0.0:8080")?
        .run()
        .await
}
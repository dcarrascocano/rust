//use jsonwebtoken::{Algorithm};
use jsonwebtoken::{Header, decode_header, decode, Validation, DecodingKey, EncodingKey, encode};

use serde::{Serialize, Deserialize};

use serde_json::{Value, json/*from_str*/};
use serde_json;

extern crate colored_json;
use colored_json::to_colored_json_auto;

use openssl::x509::{X509};

mod public_utils;
use public_utils::*;

use colored::*;

#[derive(Serialize, Deserialize, Debug)]
pub struct JWTData {
    pub header: Option<Header>,
    pub payload: Option<Value>,
    pub key: Option<String>
}

impl JWTData {
    pub fn to_token(&self) -> (Option<String>, Option<String>) {
        let key = self.key.as_ref().unwrap();
        let header = self.header.as_ref().unwrap();
        let payload = self.payload.as_ref().unwrap();
        let check_alg = format!("{:?}", &header.alg).to_string();

        if check_alg.contains("HS") { 
            (None, Some(encode(&header, &payload, &EncodingKey::from_secret(&key.as_ref())).unwrap()))
        }
        else if check_alg.contains("RS") {
            //println!("KEY {}", &key);
            let key = 
            match EncodingKey::from_rsa_pem(&key.as_bytes()) {
                Ok(value) => (None, Some(value)),
                Err(e) =>  (Some(format!("{}", e)), None)
            };
            if key.0 != None {
                return (Some(format!("{}", key.0.unwrap())), None);
            };
            let token = encode(&header, &payload, &key.1.unwrap());
            match token {
                Ok(value) => (None, Some(value)),
                Err(e) => (Some(format!("{}",e)), None)
            }
        }
        else {
            (Some("Unknown algorithm".to_string()), None)
        }
    }
}


#[derive(Serialize, Deserialize, Debug)]
pub struct Token {
    pub value: String,
    pub key: Option<String>,
    pub error: Option<String>
}

#[derive(Serialize, Deserialize, Debug, Clone)]
pub struct DataJWT {
    pub jwt: String,
    pub public_key: Option<String>,
    pub private_key: Option<String>
}

#[derive(Serialize, Deserialize, Debug, Clone)]
pub struct ValidationJWT {
    pub is_verified: Option<bool>,
    pub reason: Option<String>
}

impl ValidationJWT {
    pub fn new(sign: Option<bool>, reason: Option<String>) -> ValidationJWT{
        ValidationJWT {
            is_verified: sign,
            reason: reason
        }
    }
}

#[derive(Serialize, Deserialize, Debug, Clone)]
pub struct JWT {
    pub header: Option<Header>,
    pub payload: Option<Value>,
    pub validation: Option<ValidationJWT>,
    pub errors: Option<JWTError>
}

#[derive(Serialize, Deserialize, Debug, Clone)]
pub struct JWTError {
    pub header: Option<String>,
    pub payload: Option<String>,
    pub other: Option<String>
}

fn build_error_jwt(errors: Option<JWTError>) -> JWT {
    let validation = ValidationJWT::new(Some(false), None);
    JWT {
        header: None,
        payload: None,
        validation: Some(validation),
        errors: errors
    }
}

fn build_jwt(header: Option<Header>, payload: Option<Value>, validation: Option<ValidationJWT>, errors: Option<JWTError> ) -> JWT {
    JWT {
        header: header,
        payload: payload,
        validation: validation,
        errors: errors
    }
}

fn get_header(token: &str) -> (Option<Header>, Option<String>){
    match decode_header(token) {
        Ok(header)  => {
            (Some(header), None)
        }
        Err(e) => {
            (None, Some(format!("{}",e)))
        }
    }
}

fn parse_cert(cert: &str) -> String {
    let result: String = cert.lines().map(|x| x.trim().to_owned() + "\r\n" ).collect();
    return result;
}

fn get_payload(token: &str) -> (Option<Value>, Option<String>){
    /*let mut errors = Vec::new();
    let mut error = "".to_string();*/

    let payload_base64 = decode_base64(token);
    if payload_base64.0 != None {
        return (None, payload_base64.0);
    }
    
    let payload = decode_utf8(&payload_base64.1.unwrap());
    if payload.0 != None {
        return (None, payload.0);
    }

    let claims = parse_json(&payload.1.unwrap());
    if claims.0 != None {
        return (None, claims.0);
    }
    
    return (claims.1, None);
}

fn check_key(key: &str) -> (Option<String>, Option<Vec<u8>>) {
    if key.contains("BEGIN CERTIFICATE") {
        //let key = &key.replace(" ","").replace("CERTIFICATE", " CERTIFICATE").to_string();
        let key = parse_cert(key);
        let cert_x509 = X509::from_pem(key.as_bytes()).unwrap();
        (None, Some(cert_x509.public_key().unwrap().public_key_to_pem().unwrap()))
    } else if key.contains("BEGIN RSA PRIVATE KEY") {
       (Some("Private key of type PKCS #1 isn't valid".to_string()), None)
    } else {
        (None, Some(key.as_bytes().to_vec()))
    }
}

impl JWT {

    pub fn none() -> JWT {
        JWT{
            header: None,
            payload: None,
            validation: None,
            errors: None
        }
    }

    pub fn new(jwt: String) -> JWT {
        let tokens:Vec<&str>= jwt.split(".").collect();
        let result =
        if tokens.len() == 3 {
            
            let header = get_header(&jwt);
            let header_error = header.1;
            let header = header.0;
            
            let payload = get_payload(tokens[1]);
            let payload_error = payload.1;
            //let other = payload.2;
            let payload = payload.0;

            let error = JWTError{
                header: header_error,
                payload: payload_error,
                other: /*Some(other)*/None
    
            };

            let validation = ValidationJWT::new(Some(false), Some("No key or certificate sent".to_string()));

            build_jwt(header, payload, Some(validation), Some(error))
        } else {
            let errors = JWTError{
                header: None,
                payload: None,
                other: Some("The token isn't composed of 3 elements".to_string())
            };
            build_error_jwt(Some(errors))
        };
            return result;
    }

    pub fn validate(token: &str, key: String) -> JWT {
        let token = token.to_string();
        let token_copy = token.clone();
        let mut jwt = JWT::new(token);
        if jwt.header == None || jwt.payload == None {
            let mut validation = jwt.validation.unwrap();
            validation.reason = Some("Wrong header or payload".to_string());
            jwt.validation = Some(validation);
            return jwt;
        }
        let key  = key.to_owned();
        let alg = jwt.header.as_ref().unwrap().alg;
        let check_alg = format!("{:?}", alg).to_string();
        let token_message: (bool, Option<String>) = 
        if check_alg.contains("HS") { 
            let result = decode::<Value>(&token_copy, &DecodingKey::from_secret(key.as_ref()),
                            &Validation::new(alg));
            match result {
                    Ok(_value) => (true, None),
                    Err(e) => (false, Some(format!("{}", e)))
                }            
        } else {
            let rsa_key = check_key(&key);
            if rsa_key.0 == None {
                let result = decode::<Value>(&token_copy, &DecodingKey::from_rsa_pem(&rsa_key.1.unwrap()).unwrap(),
                                             &Validation::new(alg));
                match result {
                        Ok(_value) => (true, None),
                        Err(e) => (false, Some(format!("{}", e)))
                }
            } else {
                (false, rsa_key.0)
            }
        };
        
        if token_message.0 != false {
                let validation = ValidationJWT::new(Some(true), None);
                jwt.validation = Some(validation)
        } else {
            println!("Validacion con la firma erronea:\r\n{}", &token_message.1.as_ref().unwrap());
            let validation = ValidationJWT::new(Some(false), token_message.1);
            jwt.validation = Some(validation)
        };
        jwt
    }
}

pub fn create_token(jwt: &Option<JWTData>) -> (Option<String>, Option<String>) {
    let jwt = jwt.as_ref();

    if jwt.is_none() == false {
        let jwt_unwraped = jwt.unwrap();
        if jwt_unwraped.header.is_none() == true {
            (Some("No header".to_string()), None)
        } else if jwt_unwraped.payload.is_none() {
            (Some("No payload".to_string()), None)
        } else if jwt_unwraped.key.is_none() {
            (Some("No key".to_string()), None)
        }
        else {
            jwt.unwrap().to_token()
            //println!("EL DICHOSO {:?}", token); //RMEOVE
            //(None, Some(jwt.unwrap().to_token().1.unwrap()))
        }
    } else {
        (Some("No JSON to encode".to_string()), None)
    }
}

pub fn create_jwt(token: &Option<Token>, id: &str) -> JWT {
    if token.is_none() == false && token.as_ref().unwrap().key.is_none() == true {
        let token = token.as_ref().unwrap();
        let value = &token.value;
        let jwt = JWT::new(value.to_string());
        println!("\r\nFrom {} \r\n Received JWT : \r\n\r\n", &id);
        //let jwt_str = serde_json::to_string_pretty(&jwt).unwrap();
        let jwt_str = to_colored_json_auto(&json!(&jwt)).unwrap();
        println!("{}", &jwt_str);
        //result = jwt_str.to_owned();
        jwt 
    } else if token.is_none() == false && token.as_ref().unwrap().key.is_none() == false {
        let token = token.as_ref().unwrap();
        let key = &token.key.as_ref().unwrap();
        let value = &token.value;
        let jwt = JWT::validate(value, key.to_string());
        println!("Received JWT from {}: \r\n\r\n", &id);
        //let jwt_str = serde_json::to_string_pretty(&jwt).unwrap();
        let jwt_str = to_colored_json_auto(&json!(&jwt)).unwrap();
        println!("{}", &jwt_str);
        jwt 
    }
    else {
        JWT::none()
    }

}

pub fn show_data(data: &Option<Value>, id: &str, uid: &str) -> u16 {
    if data.is_none() == false  {
        let data = &data.as_ref().unwrap();
        
        println!("\r\n{}\r\nFrom user {} \r\nData received :\r\n", id.bright_yellow().italic(), uid.blue().bold());
        let s = to_colored_json_auto(&data).unwrap();
        
        println!("{}", s);
        200 } else { 
            500 
        }
        
}


        
    
    


    

use openssl::rsa::Rsa;
//use pem::{Pem, encode};

use openssl::x509::{X509, X509Name};
use openssl::pkey::PKey;
use openssl::hash::MessageDigest;
use openssl::nid::Nid;
use openssl::asn1::Asn1Time;

use serde::{Serialize, Deserialize};
use serde_json::Value;

use std::str;

/*#[derive(Serialize, Deserialize, Debug, Clone)]
pub enum Issuer {
    String(String),
    Object(Map<String, Value>)
}*/

#[derive(Serialize, Deserialize, Debug, Clone)]
pub struct X509Params {
    pub version: i32,
    pub subject_name: String,
    pub issuer_name: Value,
    pub not_before: Option<i64>,
    pub not_after: i64,
    pub num: u32,
}

impl X509Params{
    pub fn generate_certificate(self) -> String {
        let num = self.num;
        let rsa = Rsa::generate(num).unwrap();
        let pkey = PKey::from_rsa(rsa).unwrap();
        
        let piv_key: Vec<u8> = pkey.private_key_to_pem_pkcs8().unwrap();
        let private_key = str::from_utf8(&piv_key).unwrap();
        let pub_key: Vec<u8> = pkey.public_key_to_pem().unwrap();
        let public_key = str::from_utf8(&pub_key).unwrap();
        
        let mut subject_name = X509Name::builder().unwrap();
        subject_name.append_entry_by_nid(Nid::COMMONNAME, &self.subject_name).unwrap();
        let subject_name = subject_name.build();

        let mut issuer_name = X509Name::builder().unwrap();
        if self.issuer_name.is_string() == true {
            issuer_name.append_entry_by_nid(Nid::COMMONNAME, self.issuer_name.as_str().unwrap()).unwrap(); } 
        else if self.issuer_name.is_object() == true{
            for (k,v) in self.issuer_name.as_object().unwrap() {
                println!("{}: {}", k, v);
                let status = issuer_name.append_entry_by_text(&k, v.as_str().unwrap());
                match status {
                    Ok(T) => T,
                    Err(e) => return e.to_string()
                };
            }
        } else {
            issuer_name.append_entry_by_nid(Nid::COMMONNAME, "").unwrap();
        };

        let issuer_name = issuer_name.build();

        let not_before = if self.not_before.is_none() == false {
            Asn1Time::from_unix(self.not_before.unwrap()).unwrap()
        } else {
            Asn1Time::from_unix(0).unwrap()
        };
        let not_after = Asn1Time::from_unix(self.not_after).unwrap();
    
        let mut builder = X509::builder().unwrap();
        builder.set_version(self.version).unwrap();
        builder.set_subject_name(&subject_name).unwrap();
        builder.set_issuer_name(&issuer_name).unwrap();
        builder.set_not_before(&not_before).unwrap();
        builder.set_not_after(&not_after).unwrap();
        builder.set_pubkey(&pkey).unwrap();
        builder.sign(&pkey, MessageDigest::sha256()).unwrap();
    
        let certificate: X509 = builder.build();
        let cert = certificate.to_pem().unwrap();
        let cert = str::from_utf8(&cert).unwrap();
        let new_pkey = certificate.public_key();
        let new_pub_key: Vec<u8> = new_pkey.unwrap().public_key_to_pem().unwrap();
        let new_public_key = str::from_utf8(&new_pub_key).unwrap();
        
        //return vec!([cert.to_string(), private_key.to_owned(), public_key.to_owned(), new_public_key.to_owned()])
        println!("{}",&private_key);
        println!("{}",&public_key);
        println!("{}",&new_public_key);

        return cert.to_string();
        
        
        /*let result = CertKey{
          private_key: private_key.to_owned(),
          public_key: public_key.to_owned(),
          cert: cert.to_owned(),
          cert_public_key: new_public_key.to_owned(),
    
        };
        return result;*/
    }
}



use base64::decode;
use std::str::from_utf8;
use serde_json;
use serde_json::{Value, from_str};

trait Additional<T> {
    fn update(&mut self, element: T);
}

/*impl Additional for Vec<String>{
    fn update(&mut self, element: &str) {
        let mut result = &mut self[1..].to_vec().to_owned();
        result.push(element.to_string());
        *self = result.to_vec();
    }
}*/

impl Additional<&str> for Vec<String> {
    fn update(&mut self, element: &str) {
        self.remove(0);
        self.push(element.to_string());
    }
}

impl Additional<i64> for Vec<String> {
    fn update(&mut self, element: i64) {
        let new_element = element.to_string();
        self.remove(0);
        self.push(new_element.to_string());
    }
}
pub fn decode_base64(element: &str) -> (Option<String>, Option<Vec<u8>>) {
    let result =
    match decode(element) {
        Ok(result) =>{
            (None, Some(result))
        },
        Err(e) => {
            (Some(format!("{}",e)), None)
        }
    };
    result
}

pub fn decode_utf8(element: &[u8]) -> (Option<String>, Option<String>) {
    let result = 
    match from_utf8(element) {
        Ok(result) => {
            (None, Some(result.to_string()))
        },
        Err(e) => {
            (Some(format!("{}",e)), None)
        }
    };
    result
}

pub fn parse_json(element: &str) -> (Option<String>, Option<Value>){
    let result: serde_json::Result<Value>  = from_str(element);
    let result =
    match result {
        Ok(result) => {
            (None, Some(result))
        },
        Err(e) => {
            (Some(format!("{}",e)), None)
        }
    };
    result
}

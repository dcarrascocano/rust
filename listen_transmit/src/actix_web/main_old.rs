use actix_web::{get, post, web, App, HttpResponse, HttpServer, Responder, Result};
use serde::{Deserialize, Serialize};
use serde_json::json;
use serde_json::Value as json_value;


fn example(value: &str) -> json_value {
    json!(
        {
            "body": value
        }
    )
    
}


#[derive(Serialize, Deserialize)]
struct MyObj {
    name: String,
    surname: Option<String>
}

#[get("/")]
async fn hello() -> impl Responder {
    HttpResponse::Ok().body("Hello world!")
}

// #[get("/a/{name}")]
// async fn index(obj: web::Path<MyObj>) -> Result<HttpResponse> {
//     Ok(HttpResponse::Ok()
//     .json(
//         MyObj {
//             name: obj.name.to_string(),
//         }
//     )
//     )
// }


#[get("/a/{name}")]
async fn index(obj: web::Path<MyObj>) -> Result<HttpResponse> {
    Ok(HttpResponse::Ok()
    .json(example(&obj.name.to_string())))
}

#[post("/echo")]
async fn echo(req_body: web::Json<json_value>) -> Result<HttpResponse> {
    print!("The json is {}", req_body);
    Ok(HttpResponse::Ok()
    .json(json!({"body": req_body.0})))
    
}

async fn manual_hello() -> impl Responder {
    HttpResponse::Ok().body("Hey there!")
}

#[actix_web::main]
async fn main() -> std::io::Result<()> {
    HttpServer::new(|| {
        App::new()
            .service(hello)
            .service(echo)
            .route("/hey", web::get().to(manual_hello))
            .service(index)
    })
    .bind("0.0.0.0:8080")?
    .run()
    .await
}
use openssl::rsa::{Rsa, Padding};
use std::str;

use reqwest::header::{HeaderMap, HeaderValue, CONTENT_TYPE, AUTHORIZATION};

use serde::{Serialize, Deserialize};

use serde_json;
use serde_json::json;

use std::collections::HashMap;

#[derive(Serialize, Deserialize, Debug, Clone)]
pub struct PIN {
    pub pin: String
}

#[derive(Serialize, Deserialize, Debug, Clone)]
pub struct ManagePin {
    pub type_id: String,
    pub id: String,
    pub field: String,
    pub jwt: String,
    pub token: String,
    pub host: String,
}

#[derive(Serialize, Deserialize, Debug)]
#[allow(non_snake_case)]
pub struct ResponseLdap
{
    result: Vec<serde_json::Value>,
    resultCount: serde_json::Value,
    pagedResultsCookie: serde_json::Value,
    totalPagedResultsPolicy: serde_json::Value,
    totalPagedResults: serde_json::Value,
    remainingPagedResults: serde_json::Value
  }

pub const PK_TRANSMIT: &str = "-----BEGIN PRIVATE KEY-----
MIIJRAIBADANBgkqhkiG9w0BAQEFAASCCS4wggkqAgEAAoICAQDllLKVX8ylSMwi
54ugL/P0zI8ukSHq2KYgG+QSRnvN7K2pZkuKwUsFxLayDYoAxQGLRd1kC8DKAA9j
Q7pYGPDqiJoYpYV0HfDwJXnJnAA0Bmzf/HnCATnAGEI5g0/mojl5QCcb+eh5GC9j
i7kH911S6VjMCdECLQbNruR6yHfnkR4i33L4S8p46zwFamru4PEHALxTxl+r/oWa
GQA1DBEve9zNEoF6x7hzUOIebGtWA2dXgWyoxUYGnaeZUuFhhbLhsqfDEy5n/jaY
G/0Z9p18PFot3tuiVLiJaqsxk48sFmGttr1uyQoqPGT1TAWBas4WOnMMIWAh1T6x
+jU6T4Z60GqlrtvKYL8aWpget3eYaYr4Z+X/v4OQ/PK9NARQA39NPv8mc2t3pRUJ
xlDp7xtB8kVFZ0l5t45pnyZvDN+LSoa+peKpMpYPs21E61nsCZz7CSVea0u1hSvM
GjGcq/VgekF+MhJtMM2Vca3nz2vmo9NTS77W7D8tXeuNlDVMKARRDW+psbAsqfGi
5stn4tKyohoqMn4jk4HnXzkix5adiRum9+mTpv3yMHBx682qtcGqXWNV40kDQF49
e87gH4s53xFbF1r9vU98DGGaV9RJAdWGUTqs4J926Gdd+fw5OdQtVT/e+vAlmL/j
cZbFHcBjndbYeQmGDqH2lqZK+uQKbQIDAQABAoICAEVZrryKrNH89JVSpo3kDmiT
ZmkltD4x5bTrV/DhU8K3zRnP6d/nVddPPfT5PAfRew7sMt1m43zhmcRdkCvOlO/0
BcCON4sFBGQ2ndx2AGW2zyq2JL2jCyPo8BBZh0hfFsd2xpmf20bBNId4jJNIeOtz
y7wO9xEWMujs/N3OMtJDtvvHzf4NPwNZ486q/9RbJNP9t+YClaHqB8et568ejw5t
9xwHjA9fx2smMrgH/kCEVLJNSVqOBQI4m4koTSbdcVTWV/EImUsWsr0uLz2pjpcZ
0A/tMyGNQBVaoI4gSe6CcE0+V2jg1+48Qi8LINqgPdLfIyO17UkrFn5C29NadH+Q
YTgT6hrlStJJ2T9f9J8CkyY8lJnfkoZiPo+pj3XHZ2amCQ/I7z1f2VOUsOtObk0J
PmabuzhgHB9D2StkyUPPS3E5y2YnBOcIZ0kcOtcQ3DMVr3tBEzLkc/XBPYjRwJuX
7+/gpZfTft3uyffBJPcOweZGGCH43KCrLfUCCfDaAAF7dcqnDikTTNwDGPsCkEhc
3dXE2X1Kr1APYzCfGOF0UloymjgGZgfbGQ821/16/S2a0lUj0CLCcwM58gTWMHiN
ImkEMi9v/WGQus6bYT4TP8NjHvhQcT984qrK3B7t+fdL3glGE1U+Y5Wuq06bsR4G
XYgY98OoSFrF6sjPCDetAoIBAQD8hKI1q46Sjo+i6Vw6N8JvucT2zHYyPFl03NtR
w3qha7iK0orVBcbJcdByVrClorPEAA+2K9EpQUp+idJT1yJ+LFCGwCMWuR6Rh97Y
LHpHehMv/tZUsSNNeSEukYKUO+xvmYcTXPhcxamv7iS8cgH4z1NLwBvpDvEfIsD7
VltUOf4oEwZHOJdz4OPi2nNWhT4eeze1iJVdRHRkZ4vnTBq5o6Xf5muMzXuRJCKO
nKKPTqlGJfyv6GPG0e08v2niqs2dKcplxPHJOeHzXJfRIXzbGlFSw3sxnKNSzrrt
T3cFVZ2lRksTz89PnEiZ03pSs5F3cRuReMjdDj6NnAvCJ1onAoIBAQDovxj2odL+
E+RB/QeaqwKwiKIZF30BnZKII05jbAT/a/B/Q2hzJ6E5EduxTwo2dEGU9vbynWem
TuNpR5/4Fw2CYhrG0brm2B4uUaO8xYMXELzrAFIN6Ht9KqDRihD48Uk0zh1qLrTj
dj6tLT5v039rf3RKcqvYG05Cz5KgW9UJ73F1m0AAnUNJJxz5dD6wVpKXHmBhyYxz
cuJmOPLg1zIkhSYNSXSsVm0dumB13Jl4zMPtC7gCwJquw12yBBxtdJRgEttqYlBB
CmV0AxDaAOV23ktQma0UzHBFRznVguCDwFoaOgnIXjIva8lDQq/xAcFihUIg+BCI
9A5VwQIFN/dLAoIBAQCnKz8ffLVj/JDGbuqpd+R2hmsIX48X7q/iF5vx4xE579A5
xuw13h8dmQU/zHghDzcm2EuOdC+BukGQx7i/3DpvgPLGYkhYcLD18xbp0XgZ/Yza
R0CozKPsU/ZNl/L0Iz+BGgg8H14kPI77cxJK1sbLaqVgDovrnAKwdBeBzEel2MaS
zRVo7OQGyKjUs32Ll6XqE/Rrb0JCfmD3Xz1VICLJJrdXJHmquxXEnkFqW/OaH58c
W2WPJq93Hd6KSsXUUvLPyqy89saCSpl0vKWcwyie/Cs+OT9DNslAtq81D1hh5a35
jjGBqh86OuItQ71FJq90fSKdCXMgNJ/gq591JwwdAoIBAQDGrLFbfoGEH2i+J4bB
MW9HUCrGrnUn1uEJUtIMLl4JxpLxn5HekpqeA698KFM4bn648SOVsGWU4kxfRyO3
e74taR8gTlFz0lHZChlx/0vKeaA6HfjtJGGdU7EZjPi1+AmciME1LY99hyczXqao
gGqJUa/sCPzFqXFhuJyaRCmTpJA+avr0S0nvfPLPwbntfsj9NJHjh09cEVpPFhfF
dAy6feogd26f2dqlAL5sfZWIN4qbo2A9ATpJKj2lwrqdeGFbrV0c77U//+bgY+mT
niZCFLlHoRbM5UKE8KrepiyOari/IIHPRxEhx6lyv88+NFhmiAgN6pL6xRIjg3D9
EWwHAoIBAQC0ulVPTtWL+VbVWfTwYfiLNLRuG7HHbL4unooGAgB0KbPfB6oGI2Zu
sqqF/htZ+iVkq2HSsWf2xH11P9oT7E9YDhAkiH5fjEYCnXS7JNty5bfDpmos2oO+
XwQfJDvDqZYneuhR20E7MJnYwusKViQ/fvRC85GD36PmFHKk/W13eQEO/6TeydRI
rPGnQXhhRsy6p6yITAaraenrRmVlhB/rLayZhw41vQWW1PfSFhCYzhUzmv+KJJiF
zpLUJ5MZfcYZ323toyJgxE1dXHbyyO/JsHYTyvewDDy7hmjMrrcCvE55TWF7S+ek
bMr8i5VY1PBt1zgFHuEs/3oUZ63HMUJv
-----END PRIVATE KEY-----";

pub const PK_LDAP: &str = "-----BEGIN PRIVATE KEY-----
MIIJQwIBADANBgkqhkiG9w0BAQEFAASCCS0wggkpAgEAAoICAQDMjVG1XQVnI0j1
JFTP+TFSB54nVShwx0+XYseQQqN6cxSSVviHKubZLf2uKroHPPJ3TFSOe0lQIxlT
xOucFVplK2odyHY16KtPVDYmHbnedCVE3anbFtjfW3KirLe1y4lKIjIamQxzoJ2D
sPp1reSl19qomriiZ7W7JPFBCN1joWScbvCCm7c3992H99HxbIr9V6lblBLUvHye
fyyCboTa4SC0CQfWF0getDGvOmu4i5IzygUjLp5li5H+EF0T2IYSfYfa68VHYpWE
rv+ml5sVRuJkjL0Nny20G3IfOoffODcjhswmbS6TeeQ8mbjO5ju+l+a6+eBD/X8P
U/ClumyJDoY88XUR/T1IFqbCjdNNeCdsw4IDSLpWzTSBefl7nB4ReWajgLmwCR3w
xiA2xwc1tXpD2UM4HKwmWa6zG9f5YuIgVhXLaO2HoDJT5NgaLYu3tcmsFxYgRHh8
D9o0cmNhcm+to/a8Rwe/zS7FPqOkzHOfycnKf7SkYcieFbtn1HpuSSZJj/zF3V54
1mOGSMy/R1cwwWZP/Z2jvOIisGd1Bknd//AEugbBrQmH9iYHquaFrbc/RyliFrtZ
p4JV9qLbBisYvaMWzmGwr7QCcD2n2Uc6BrOfFNJIS2p3iuOLfjzcaPE9tabGKInS
jJEx1bNnljYgLJGKRApYVlhqWx8pAQIDAQABAoICACmRVldqzdxFuV/HwEx7MWCg
F62aiptQhYYYFsHkKOCLXlTWw66EM2FTDUhFeoRaCFPh+RTzTNSyLUVusQqI9FO2
3NusjwkpfRaj4FUimVDxfVlfSlMxc2UAam1QeAzDjeS4ykHJdFRTAW9vmZYsfliN
Yuv8dSTbunyLZ1N1/7dtZsq7aLjB+BuxjR1CaTGMgzo0hEAN+T2uCA0VfvAKkYkB
UuoDI8qH9Kn4qFgZoSZhhpZrqVua76lnEYX1wVR1HdnIFMGSRAbbq9LI1T7/dA6A
1yTjzM9l153QLgLWnCHqhJn9nKL6cQqbLJx8CKhS9TUoHUVF3iMy8o0k27RyC/f5
KKZGTISKhXkKHXWmVIU4Htis/vRoIoMpyW6mDb2FfF3m14dwTcXDhE22HuHv8Ihe
TdU8exw9WFlNhV71P5q3mncfDr/I2JZadpuwABkgaqX8oPZ3THowLSc1TVuhWlJC
oiwtvm5LTY3HvfQz5lfkWxlcNg9icT8s9SkmaU0HaMu/1LEnuhawB3Z/kPLn8rJg
2v+fhx7g0wH4M+IQxr1aFEiIcpT2q1CqJmu3KbY8Yxn0oPdavTq99xfX13JHAgwg
KzVrNn8f58LURkqeVOTNjJmD2SVnkBswFDjxMIgRgZ96i+DSPkzaKjB3Pp5UhNN8
I0SLDyZWylnffxfV/PoBAoIBAQDqoY2/W0h2/EnrwnpLdxChN3Rg/f/xhM/ppQSb
E2VjTQUJBj0yzlWj0g0i65c9D7Kjerd8S3VPgX0lPWi53CmxKZsxgad4Csgduf31
ls/xt8QQCgd9IWSrHdcv9ldb5O7QTq0lrCrewRAPbRMEWNrbanCrCOQi41gzLHJl
Qda2OflazdAh1yoc/XyLxNqJwlAaDzSe4gPuvW+Uzfmalc7qk3UDOMsASTIfINCf
PlpHi3GYbypbPNbLsbGwISUEKwsBtjIEWAqghVmGevDLOaN915chVIiiBvubH6z1
oQn74JNkT1xkL87gR5SFluUXl9GtM7uSC5iSDAFQzHDN1eTxAoIBAQDfLng9nmdR
ZI/q312tO2BjX3Fy3I/BpLiXbfS/57+BtX7w6BBYqRLxCjPwd42vXTDmIShJbnZ7
HYn9m69wz/J2iEwP+Z3uphk33qvYgAw2mpXRrA3HnA1HycJ3xbSXqtu4r/XYi+wk
CmKY5Mv89o/DE3RBjoHcGB6BpKBWWlxlwuoFInD26FIDYlv/bv2xK4dKPd1oZOFg
YTUOUu9KeNcb35J0VSpmlI24I2xN64iEqURzfZis8O/NQF2slLRDimXUkWV3XOrq
zAaofXuCCnQu0Fdr4AFn4qjCS54TcZ1D5QMpZbokJ+dRqFaG8AsYstM1znEXeQnW
4v5LnKwklkURAoIBAQDS6pFCoGs+0sIclgFbj1e6pOAQRKRsVwiTrsy6HrB4mIbb
OumOn17q2esHJiMZN+BNBwakTCzTYMK/1DktQpWT9aPV2AhUZ9TGrCh4Oa8KSjiM
doeO25LkI/oJ2ndjWyO2y9LoV6FeBs1KILIEqV9r+H35sg9qsVXbAqx/C7VYsDrQ
fs0HbqUj1Fa+SiJtzeg11M9nuCLy3QXODW7AoMSVqCJ61/XcdvAAfX4seF+titN9
xQidZc5s43qoqtLcsVi+nCrQLti2vcjhk+ibt1+Zw1DPHG+Kv540L0BKug4AhzvX
42NZiIR+proDcMbV85sACM4nlhb7yqDwIieRUSLxAoIBAQCZ+7ToRr9gL/MACUbX
u+zgAz0CE913rI/TowDH2zyNYM0JqAOHOHgiKHj3g3UVNkYDsYLbT0Sd4x6PBPiB
7GeMkrNwHjhyLNWfb5m/UXGbxwJxXLVRMIEVM8Zh3t2f+zmH7pe/5aJyp1upw8pe
aTt9S5HdnVZJpbZ+mjgp/ERdzaKrEj1Ajb/wJ8/zp/+PWsoIVjDJ0ziQx+DUVjJV
fpRj3dhtPUl45yv0jTQver+BUEwzJsfNIptBURSQezT4mKdzhzGrrkdC4qwBWjp5
2X6ZwhNfEXnG1GK9U3DoV247JuoMiOxp/8Ig2/V/OmVdkBwbtqjemhtI0OrE/n7i
628RAoIBACfrJrULhOWzfPBRH7SYHVP2MwkmOCuhzjBDTATErV1cK+rwIyGdLYdL
WB6hRiNqCxVjAXfSIvKjR38rRiecgdTmOJK0KI3RDT6r9Fcst3dFbNSZ5Y9wtz7z
/dy+EQOTXKT/Z/hvd32KaQaOSdScaPxDv+QmIVF4uwYXOgbBMdfMVYcuizJYp8dh
24RlaU1rWYctx2xUCOOWm8I7sUsjQwJzYa9A7cQFzxzZ+TXHV+BTVC5eJfi57aHH
ggIMYnVF5T0fDjiz0/Ume4jwqsM5e/vg0C6I8NyIweVoJxbOVEL7sPhzH29wr61m
/IykDDGFbXvZ+pyp94OgaULoscwpK4A=
-----END PRIVATE KEY-----";

pub const PUB_KEY_TRANSMIT: &str = "-----BEGIN PUBLIC KEY-----
MIICIjANBgkqhkiG9w0BAQEFAAOCAg8AMIICCgKCAgEA5ZSylV/MpUjMIueLoC/z
9MyPLpEh6timIBvkEkZ7zeytqWZLisFLBcS2sg2KAMUBi0XdZAvAygAPY0O6WBjw
6oiaGKWFdB3w8CV5yZwANAZs3/x5wgE5wBhCOYNP5qI5eUAnG/noeRgvY4u5B/dd
UulYzAnRAi0Gza7kesh355EeIt9y+EvKeOs8BWpq7uDxBwC8U8Zfq/6FmhkANQwR
L3vczRKBese4c1DiHmxrVgNnV4FsqMVGBp2nmVLhYYWy4bKnwxMuZ/42mBv9Gfad
fDxaLd7bolS4iWqrMZOPLBZhrba9bskKKjxk9UwFgWrOFjpzDCFgIdU+sfo1Ok+G
etBqpa7bymC/GlqYHrd3mGmK+Gfl/7+DkPzyvTQEUAN/TT7/JnNrd6UVCcZQ6e8b
QfJFRWdJebeOaZ8mbwzfi0qGvqXiqTKWD7NtROtZ7Amc+wklXmtLtYUrzBoxnKv1
YHpBfjISbTDNlXGt589r5qPTU0u+1uw/LV3rjZQ1TCgEUQ1vqbGwLKnxoubLZ+LS
sqIaKjJ+I5OB5185IseWnYkbpvfpk6b98jBwcevNqrXBql1jVeNJA0BePXvO4B+L
Od8RWxda/b1PfAxhmlfUSQHVhlE6rOCfduhnXfn8OTnULVU/3vrwJZi/43GWxR3A
Y53W2HkJhg6h9pamSvrkCm0CAwEAAQ==
-----END PUBLIC KEY-----";

pub const PUB_KEY_LDAP: &str = "-----BEGIN PUBLIC KEY-----
MIICIjANBgkqhkiG9w0BAQEFAAOCAg8AMIICCgKCAgEAzI1RtV0FZyNI9SRUz/kx
UgeeJ1UocMdPl2LHkEKjenMUklb4hyrm2S39riq6Bzzyd0xUjntJUCMZU8TrnBVa
ZStqHch2NeirT1Q2Jh253nQlRN2p2xbY31tyoqy3tcuJSiIyGpkMc6Cdg7D6da3k
pdfaqJq4ome1uyTxQQjdY6FknG7wgpu3N/fdh/fR8WyK/VepW5QS1Lx8nn8sgm6E
2uEgtAkH1hdIHrQxrzpruIuSM8oFIy6eZYuR/hBdE9iGEn2H2uvFR2KVhK7/ppeb
FUbiZIy9DZ8ttBtyHzqH3zg3I4bMJm0uk3nkPJm4zuY7vpfmuvngQ/1/D1Pwpbps
iQ6GPPF1Ef09SBamwo3TTXgnbMOCA0i6Vs00gXn5e5weEXlmo4C5sAkd8MYgNscH
NbV6Q9lDOBysJlmusxvX+WLiIFYVy2jth6AyU+TYGi2Lt7XJrBcWIER4fA/aNHJj
YXJvraP2vEcHv80uxT6jpMxzn8nJyn+0pGHInhW7Z9R6bkkmSY/8xd1eeNZjhkjM
v0dXMMFmT/2do7ziIrBndQZJ3f/wBLoGwa0Jh/YmB6rmha23P0cpYha7WaeCVfai
2wYrGL2jFs5hsK+0AnA9p9lHOgaznxTSSEtqd4rji3483GjxPbWmxiiJ0oyRMdWz
Z5Y2ICyRikQKWFZYalsfKQECAwEAAQ==
-----END PUBLIC KEY-----";

pub fn encrypt(text: &str, key_pem: &str) -> String {

    let rsa_public = Rsa::public_key_from_pem(key_pem.as_bytes()).unwrap();
    let mut encrypted_text = vec![0; rsa_public.size() as usize];
    rsa_public.public_encrypt(text.as_bytes(), &mut encrypted_text, Padding::PKCS1).unwrap();
    
    let result = format!("{:?}", &encrypted_text).replace("[", "").replace("]", "");
    return result.to_owned();

}

pub fn encrypt_private(text: &str, key_pem: &str) -> String {

    let rsa_private = Rsa::private_key_from_pem(key_pem.as_bytes()).unwrap();
    let mut encrypted_text = vec![0; rsa_private.size() as usize];
    rsa_private.private_encrypt(text.as_bytes(), &mut encrypted_text, Padding::PKCS1).unwrap();
    
    let result = format!("{:?}", &encrypted_text).replace("[", "").replace("]", "");
    return result.to_owned();
    //return str::from_utf8(&encrypted_text).unwrap().replace("\u{0}", "").to_owned();

}


pub fn decrypt(text: &Vec<u8>, key_pem: &str) -> String {

    let rsa_private = Rsa::private_key_from_pem(key_pem.as_bytes()).unwrap();
    let mut decrypted_text = vec![0; rsa_private.size() as usize];
    rsa_private.private_decrypt(text, &mut decrypted_text, Padding::PKCS1).unwrap();
    
    return str::from_utf8(&decrypted_text).unwrap().replace("\u{0}", "").to_owned();

}

pub fn decrypt_public(text: &Vec<u8>, key_pem: &str) -> String {

    let rsa_public = Rsa::public_key_from_pem(key_pem.as_bytes()).unwrap();
    let mut decrypted_text = vec![0; rsa_public.size() as usize];
    rsa_public.public_decrypt(text, &mut decrypted_text, Padding::PKCS1).unwrap();
    
    return str::from_utf8(&decrypted_text).unwrap().replace("\u{0}", "").to_owned();

}

fn generate_request(method: &str, url: &str, token: &str) -> Option<reqwest::blocking::RequestBuilder> {
    
    let authorization = format!("Basic {}", token);
    let mut headers = HeaderMap::new();
    headers.insert(CONTENT_TYPE, HeaderValue::from_static("application/json"));
    headers.insert(AUTHORIZATION, HeaderValue::from_str(&authorization).unwrap());
    
    match method {
        "get" => Some(reqwest::blocking::Client::new().get(url).headers(headers)),
        "patch" => Some(reqwest::blocking::Client::new().patch(url).headers(headers)),
        _ => None
    }
    
}

pub fn check_pin(type_id: &str, id: &str, field: &str, pin: &str, token: &str, host: &str) -> serde_json::Value {
    //let mut result = HashMap::new();
    //let mut HashMap<&str, i32>

    let type_ldap = 
    match type_id {
        "email" | "mail" | "E-mail" => "mail",
        "doc_id" | "docId" => "title",
        "alias" => "givenName",
        _ => ""
    };
    let url = format!("{}:8636/api/users?_queryFilter={}+eq+'{}'&_fields=_id,{}", host, &type_ldap, id, field);
    let request = generate_request("get", &url,token).unwrap();

    serde_json::json!(request.send()
    //.await
    .unwrap()
    .json::<ResponseLdap>()
    //.await
    .unwrap())

}

pub fn assign_pin(type_id: &str, id: &str, field: &str, old_pin: &str, new_pin: &str, token: &str, host: &str) -> serde_json::Value {

    let check = check_pin(type_id, id, field, old_pin, token, host)/*.await*/;
    let uuid = &check["result"][0]["_id"].as_str().unwrap();
    if &old_pin == &check["result"][0][&field].as_str().unwrap() {
        let url = format!("{}:8636/api/users/{}", host, &uuid);
        let field = format!("{}", field);
        let body = json!([{
            "operation": "replace",
            "field": &field,
            "value": &new_pin
        }]);

        let request = generate_request("patch",&url,token).unwrap();
        serde_json::json!( request.json(&body)
            .send()   
            .unwrap()
            .json::<serde_json::Value>()
            //.await
            .unwrap()
        )
    } else {
        serde_json::json!(false)
    }
    
}

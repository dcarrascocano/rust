use openssl::rsa::{Rsa, Padding};
use std::str;

use reqwest::header::{HeaderMap, HeaderValue, CONTENT_TYPE, AUTHORIZATION};

use serde::{Serialize, Deserialize};

use serde_json;

use std::collections::HashMap;


#[derive(Serialize, Deserialize, Debug)]
#[allow(non_snake_case)]
pub struct ResponseLdap
{
    result: Vec<serde_json::Value>,
    resultCount: serde_json::Value,
    pagedResultsCookie: serde_json::Value,
    totalPagedResultsPolicy: serde_json::Value,
    totalPagedResults: serde_json::Value,
    remainingPagedResults: serde_json::Value
  }

  pub const PRIVATE_KEY: &str = "-----BEGIN PRIVATE KEY-----
  MIIJQwIBADANBgkqhkiG9w0BAQEFAASCCS0wggkpAgEAAoICAQDCShoHF5UDcyPz
  gJvkt3ixCNYxEzOv6HATAXInS+cTF+8SVNj1SA8g/85B4GfPv6DHeQN9+SXzAhFf
  zfyKH7sqvkwtZVsu1Lje6KaX/Vzsnh7tT5xa95H40qaaiu/j7MNeBU6ehHWZ/GfE
  qK7gXQU1bfgmu7DqIBQ4OZWH20YngNPvT6uQT3T0Fcm01GuGQExhtlRAKe4J4+tD
  75uEr+DMiGpH6ufB8kou+TCVmJRksKfDmjfQwxKevgU9/RvsbCgu+AT+wW9sp7Bn
  ltjdFcAyBNNmhmB+j095lRtP72PDZKdiW/tyryr7kXZJlLYbhuRuk8QsC0LacAiY
  CqHl4BGt3dNvPFsMSv1JkGYio9wMo0tViX1aXmgLEDpb+e1bqLNR6Ch5ftL5nm3r
  sxSDyEM9n+WKHZNeOdiIavaAI0Zte53RuN+Ksfez6Lg9pP3xw0OdVYqGql0Mfyvx
  QRGw6IKI9kXIAPmAaRxF61PbFSxHHZuty5oQ1q1xXS0ycCDHTlprKwheFRzoQmr/
  SltQKBENUjtsU/9STZOMvNs0t/h4LQhb77i71vqbp/4IbH9Z4z+xUDvmVaGZJPD7
  qRuxt2RFUj4mzh/CDm8iY4jJuXZ0D+QQvHC7qTfLJSPexsOPQMVyWozV6gmxjGQA
  Vc95HaxZdNTerL95KBpRep5dYXFFgwIDAQABAoICAQCMe34kYKXLj8aP/A88qnjn
  MSeG5oZ6DAx1gZcj2NslUCn6TEVv3YGq76wYsAUAD2RWtyaSQMNy2azsZAnVaaow
  9tYoAOCUlRVzgC9tOLKnGJHuL6EzmUz/6Hi0x/87A+gjpJy7O7W4+OujJAEGk9v/
  TRugDhiWwUk9ek2npSIt0BXuNOItf3ZmQxbM3Mc+lGFmSarTlj3bAuULA0alA3L9
  ESogBDc9Yk7HdkDt+hxaTe2QHoN7PIHLLESoFMrLAU1w4L7j4cQKgZmo3UbDXUXS
  JPEiccl12XOyyuCrxMGS98Wb9jxzKTtgGUa5X4DpmHi1h7zRx/a38UgVf6Kej4zX
  stcMKhpyx6kVBg/ww7iTnL/em37/7qfEoCQ4Zj2pSdIrj9LLmZqaa+WIiBnQOG7m
  /hU0BSdNWrRMIlMdQq7u6leSnS50EZtOBlQ82JlwS6mN+GpL//O8XxnFnOMEG1ja
  e0gEYh92OA+JSg/Z+WcFGoOfOH6p/iG6ipWxaMOX2YGtjOr4qZB8nFfD3uHPNW1h
  cmpJVHTceTvEkMe8tDnt/vsUmnPWz+tzXiWzlYON7roK7MCDJymmeJYV8QtbWg3Q
  tGNXebym/2NFpyWW1eH2VNgv4lDr9Za4n6KOdfII5RI+8fxArq/hHnH4UuuRYoJT
  OtaVPF/dDcxOAMQGka8uiQKCAQEA6v1Yr8xccdigbR3stGO597hsmcmVQr+LDxHh
  XJVawK86zPkmhfQBVU7U8mWIGlrY89aYBT1JJJtHWFPP9/5gH78OdzuJdcEeCPse
  lL1K5lzcygYWTPc8BT8hyPNlWjpfwuvK7Pm/JJyvvM8ZxL9KVUVGI8DXQhaDPEAo
  doiTUNulg+24DXWZte8WquqzGDVrat769JiqCgZ+S9XB9V0EF9FJOU0Yzfmz8RHL
  x0lpAKnbNCGUcUwsyNlz8uZOgPnHjMRoyPJhYw6DrS0vxbulqD7CFXlZqwr59KgF
  r3z8CQgjyLbeoeacM8PPdcDFqEFb3zKnVyz8kGemQOdqf5UXRQKCAQEA06ksUdHq
  AO/+wMrfTNeK4pKE3flSlKJUPEFHMr1FzhJQAOaO0oYwGuvGeH7sG8YzGxjC2u6C
  +h8ZapbkR+8xBgZKsax4Cvau7pPYvKUFrbZ+wXEg9yvCXGJkwkiRgM2kfIBwUdym
  F08rSyDnXILBjdIt1Brh1OD1xAxCUDiHrVaKaFpvoupi9a0S99hkRt3b3y7054tC
  J2zZTR8GkssRfw6cpxYVjhJ7lXG4ENMEPXgATvadzqIz/vlz2ME64cmja0n0O209
  bKkN24ZGQbafq57SDN21njylEnOHJU7pZoR4DYmE+WwQf6FLU1q8sjvFLu+71PmH
  QNDXh4/fNsZyJwKCAQEAnDXaNNMXSR8GVkMkTS5WO5S/rOPSc1K9cEEknNU0RQ63
  efbOYxG3qSwEZzaTaWGnA0FOQX1iGBGZNeBRSbwxzijFxwEu+bZs7u9wftPu7IAi
  zbGKNZPMtHgpT1ptr7NKYVs5xJqpM/9s24iYyq+XDGRGohHZhrdMekr0GwfjVkcN
  Gkx2o4vfqI/Nc8UdVSR9ycd88AhkUNHMN0Fb9G3nAKZBlPPpX1N1CPYZjtsAEE7Z
  xyfjdAV9Z+8WIwxyd06UFsGiHqLxllXZts9pCHsbPmKpxniq4Qck7jybuvLM4Hwm
  vZDDIsEYaVjrmo2RV65LEm6ry0m+g4hDfrXXmARWOQKCAQAOKx3nYDjc1TPlbe8i
  sq7+WbaBMWZgwrkGy4vbjqX0lG88kDJxT0A7HRtNB5oAhfT31GF+zEaCPcCyzOT9
  F7rdX2Byokvq1/Z/y+/POPC3Xyb/Bfah9RDupDuAceEjkSB4oRRjtnR7zPXl8o7q
  N2qDdtVQhHgniuOIuxVZT19tsARV1e/xJOV48pv5cUfNIp7MQtVsBntoLjYhnnkP
  LH7AVZcQ7D6HIx+YsmMS5T08bCMBJisMIPmAEOy7TrwFTz0cOngZ3kclVe+yiTt8
  vJ/Vmi0PZWv56q9WG9upGK4xCZ8LsO/wzlYQZRiovMMy3yNk7/FP4WA3a4Nx9KSJ
  gyEzAoIBAGV0gLMy4Op+BI2qz0pA1eC8CtGnbrX3hHUnW+0xSUu0GSFD735ScHDN
  TTmRlN/TvOHsBhscjNT99JgKknRiAmG1WuUT+jLhGBYKF3c06uvD2qgXqwJxZ0TF
  X291FW3HcLfScgex+nYiOR4wVbOA/PzW2Qvw3Yd84PC3nzEWGdrPibn1fHTyPt9g
  9SWGxVKb08yqi8gViNwAJaO8wJvepNK1FdczakOc+7HUNZxUOT28usZ6yE37jodn
  n5y8e9LV5GBWLt+PFFl2nzIjEQFRWi7nmGUit5kkFgCp8KS1CLBjzAp315DI/Qsl
  dhkiY3Do094IZinlnsNbo6eAE1psiXk=
  -----END PRIVATE KEY-----";
  
  pub const PUBLIC_KEY: &str = "-----BEGIN PUBLIC KEY-----
  MIICIjANBgkqhkiG9w0BAQEFAAOCAg8AMIICCgKCAgEAwkoaBxeVA3Mj84Cb5Ld4
  sQjWMRMzr+hwEwFyJ0vnExfvElTY9UgPIP/OQeBnz7+gx3kDffkl8wIRX838ih+7
  Kr5MLWVbLtS43uiml/1c7J4e7U+cWveR+NKmmorv4+zDXgVOnoR1mfxnxKiu4F0F
  NW34Jruw6iAUODmVh9tGJ4DT70+rkE909BXJtNRrhkBMYbZUQCnuCePrQ++bhK/g
  zIhqR+rnwfJKLvkwlZiUZLCnw5o30MMSnr4FPf0b7GwoLvgE/sFvbKewZ5bY3RXA
  MgTTZoZgfo9PeZUbT+9jw2SnYlv7cq8q+5F2SZS2G4bkbpPELAtC2nAImAqh5eAR
  rd3TbzxbDEr9SZBmIqPcDKNLVYl9Wl5oCxA6W/ntW6izUegoeX7S+Z5t67MUg8hD
  PZ/lih2TXjnYiGr2gCNGbXud0bjfirH3s+i4PaT98cNDnVWKhqpdDH8r8UERsOiC
  iPZFyAD5gGkcRetT2xUsRx2brcuaENatcV0tMnAgx05aaysIXhUc6EJq/0pbUCgR
  DVI7bFP/Uk2TjLzbNLf4eC0IW++4u9b6m6f+CGx/WeM/sVA75lWhmSTw+6kbsbdk
  RVI+Js4fwg5vImOIybl2dA/kELxwu6k3yyUj3sbDj0DFclqM1eoJsYxkAFXPeR2s
  WXTU3qy/eSgaUXqeXWFxRYMCAwEAAQ==
  -----END PUBLIC KEY-----";

pub fn encrypt_private(text: &str, key_pem: &str) -> String {

    let rsa_private = Rsa::private_key_from_pem(key_pem.as_bytes()).unwrap();
    let mut encrypted_text = vec![0; rsa_private.size() as usize];
    rsa_private.private_encrypt(text.as_bytes(), &mut encrypted_text, Padding::PKCS1).unwrap();
    
    let result = format!("{:?}", &encrypted_text).replace("[", "").replace("]", "");
    return result.to_owned();
    //return str::from_utf8(&encrypted_text).unwrap().replace("\u{0}", "").to_owned();

}

pub fn decrypt_public(text: &Vec<u8>, key_pem: &str) -> String {

    let rsa_public = Rsa::public_key_from_pem(key_pem.as_bytes()).unwrap();
    let mut decrypted_text = vec![0; rsa_public.size() as usize];
    rsa_public.public_decrypt(text, &mut decrypted_text, Padding::PKCS1).unwrap();
    
    return str::from_utf8(&decrypted_text).unwrap().replace("\u{0}", "").to_owned();

}

fn generate_request(method: &str, url: &str, token: &str) -> Option<reqwest::blocking::RequestBuilder> {
    
    let authorization = format!("Basic {}", token);
    let mut headers = HeaderMap::new();
    headers.insert(CONTENT_TYPE, HeaderValue::from_static("application/json"));
    headers.insert(AUTHORIZATION, HeaderValue::from_str(&authorization).unwrap());
    
    match method {
        "get" => Some(reqwest::blocking::Client::new().get(url).headers(headers)),
        "post" => Some(reqwest::blocking::Client::new().post(url).headers(headers)),
        _ => None
    }
    
}

pub fn get_id_by_mail(type_id: &str, id: &str, field: &str, token: &str, host: &str) -> serde_json::Value {
    
    let type_ldap = 
    match type_id {
        "email" | "mail" | "E-mail" => "mail",
        "doc_id" | "docId" => "title",
        "alias" => "givenName",
        _ => ""
    };
    let url = format!("{}:8636/api/users?_queryFilter={}+eq+'{}'&_fields=_id,{}", host, &type_ldap, id, field);
    let request = generate_request("get", &url,token).unwrap();

    serde_json::json!(request.send()
    //.await
    .unwrap()
    .json::<ResponseLdap>()
    //.await
    .unwrap())

}

pub fn unlock_user(uid: &str, app: &str, host: &str, token: &str) -> serde_json::Value {
    let url = format!("{}/api/v2/mng/support/unlock/application?aid={}&uid={}", host, app, uid);
    let request = generate_request("post", &url, token).unwrap();

    serde_json::json!(request.send()
    //.await
    .unwrap()
    .json::<serde_json::Value>()
    //.await
    .unwrap())
}
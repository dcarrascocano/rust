use serde::{Deserialize, Serialize};
use serde_json;
use jsonwebkey_convert::*;

use std::collections::HashMap;

use std::sync::Mutex;

#[derive(Deserialize, Serialize, Debug, Clone)]
pub struct Keys{
    pub key: std::vec::Vec<JwkData>
}

pub struct SharedDataJWK {
    pub shared_data: Mutex<HashMap<String, String>>,
}

#[derive(Serialize, Deserialize, Debug)]
pub struct DataOriginJWK {
    pub kid: String,
    pub public_key: String
}

#[derive(Deserialize, Serialize, Debug, Clone)]
pub struct JwkData {
  pub kid: String,
  pub kty: String,
  #[serde(rename = "use")]
  pub jwk_use: String,
  pub n: String,
  pub e: String
}

fn parse_cert(cert: &str) -> String {
    let result: String = cert.lines().map(|x| x.trim().to_owned() + "\r\n" ).collect();
    return result;
}

impl JwkData {
    pub fn new() ->JwkData {
        JwkData {
            kid: String::from(""),
            kty: String::from(""),
            jwk_use: String::from(""),
            n: String::from(""),
            e: String::from("")
        }
    }

    pub fn generate_jwk(&mut self, kid: &str, jwk_use: &str, p_key: &str) {
        let p_key = parse_cert(p_key);
        
        let jwk_data = RSAJWK {
            kid: Some(kid.to_string()),
            jwk_use: Some(jwk_use.to_string()),
            pubkey: load_pem(p_key.as_bytes()).unwrap()
            //pubkey: load_pem(value.as_bytes()).unwrap()
        };
    
        let jwk = jwk_data.to_jwk();
    
        let json_str = match jwk {
            Ok(v) => v.to_owned(),
            Err(e) => format!("{:?}", e),
        };
    
        let result: JwkData = serde_json::from_str(&json_str).unwrap();
        self.kid = result.kid;
        self.kty = result.kty;
        self.jwk_use = result.jwk_use;
        self.n = result.n;
        self.e = result.e;
    }
}




use glob::glob_with;
use glob::MatchOptions;

use serde_json::{Value};
use serde::{Serialize, Deserialize};

extern crate tera;
use tera::{Context, Tera};

use std::sync::Mutex;

use std::collections::HashMap;

pub struct SharedDataHtmlRender {
    pub shared_data: Mutex<HashMap<String, String>>
}

#[derive(Serialize, Deserialize, Debug)]
pub struct Html {
    pub page: Option<String>,
    pub params: Value,
    pub url: String,
    pub path: Option<String>
}

pub fn build_context(info: &Option<&Html>) -> String {

    let info = info.unwrap();
    let page = info.page.as_ref().unwrap();
    let path = info.path.as_ref();
    let path = 
    if path.is_none() == false {
        format!("{}/**/*", path.unwrap()) }
    else {
            "templates/**/*".to_string()
        };
    
    let params = &info.params;

    let options = MatchOptions {
        case_sensitive: false,
        require_literal_separator: false,
        require_literal_leading_dot: false,
    };
    for entry in glob_with(&path, options).unwrap() {
        if let Ok(path) = entry {
            println!("{:?}", path.display())
        }
    }

    
    let tera = match Tera::new(&path) {
        Ok(t) => t,
        Err(e) => {
            println!("Parsing error(s): {}", e);
            ::std::process::exit(1);
        }
    };
    //let tera = Tera::new("templates/**/*").unwrap();
    //let mut context = Context::new();
    let rendered = tera.render(&page, &Context::from_serialize(params).unwrap()).expect("Failed to render template");
    println!("PAGE \r\n, {}", &rendered);
    return rendered.to_string();
}



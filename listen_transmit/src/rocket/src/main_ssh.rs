mod encrypt_request_unblock_user;
use std::io;

use std::net::TcpStream;
use ssh2::Session;


//Read from file

//Claim for email

fn main() {

    let tcp = TcpStream::connect("192.168.1.20:22").unwrap();
    let mut sess = Session::new().unwrap();
    sess.handshake(&tcp).unwrap();

    sess.userauth_password("root", "Artema_6891").unwrap();
    assert!(sess.authenticated());

    println!("Introducir email:\r\n");
    let mut input = String::new();
    io::stdin().read_line(&mut input);
    let identity = encrypt_request_unblock_user::get_id_by_mail("mail", &input, "mail",
    "dHJhbnNtaXQ6T0JPejRScDNack9icjZFZHVDdjM=", 
    "http://localhost");
    println!("{:?}", &identity);

    if identity["resultCount"] == 1 {
        //unlock user
        let uid = &identity["result"][0]["_id"].as_str().unwrap();
        let unlocked_user = encrypt_request_unblock_user::unlock_user(&uid, "splayt", "https://iam-admin.gid.sandigital.innaacc.dev.corp",
        "TSToken 27b5e0e5-6d2d-4736-9be4-d412b645e044; tid=custom_user_data_token");
    } else {
        println!("This user doesn't exist");
    }
}

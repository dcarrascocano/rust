#![feature(proc_macro_hygiene, decl_macro)]

#[macro_use] extern crate rocket;
#[macro_use] extern crate rocket_contrib;
use serde::{Serialize, Deserialize};
use rocket_contrib::json::{JsonValue,Json};
use rocket::State;
use rocket::response::content;
use rocket::config::{Config};
use rocket::http::Method;

use std::vec::Vec;
use serde_json::Value;
use serde_json::json as json_value;

mod jwt_lib;
use jwt_lib::{JWT, DataJWT, JWTData, Token, create_token, create_jwt, show_data};


mod jwk_lib;
use jwk_lib::{DataOriginJWK, SharedDataJWK, Keys, JwkData};

use std::collections::HashMap;


use chrono::{DateTime, Utc};

use std::sync::Mutex;

mod render_html;
use render_html::{Html, build_context, SharedDataHtmlRender};

mod certificate;
use certificate::{X509Params};

use colored::*;

mod encrypt_request;

// use rocket::http::Method;


//use rocket_cors::{
//    AllowedHeaders, AllowedOrigins, Error,
//    Cors, CorsOptions
//};

//fn make_cors() -> Cors {
//    let allowed_origins = AllowedOrigins::some_exact(&[ // 4.
  //      "http://localhost:8080",
    //    "http://127.0.0.1:8080",
//        "http://localhost:8000",
 //       "http://0.0.0.0:8000",
//        "http://0.0.0.0:8080"
//    ]);
//
//    CorsOptions { // 5.
//        allowed_origins,
//        allowed_methods: vec![Method::Get, Method::Post].into_iter().map(From::from).collect(),
//        allowed_headers: AllowedHeaders::some(&[
//            "Authorization",
//            "Accept",
//            "Access-Control-Allow-Origin",
//        ]),
//        allow_credentials: true,
//        ..Default::default()
//    }
//    .to_cors()
//    .expect("error while building CORS")
//}

pub struct SharedDataJson {
    pub shared_data: Mutex<HashMap<String, Value>>,
}

/*trait Additional<T> {
    fn update(&mut self, element: T);
}*/

struct SharedData {
    shared_data: Mutex<HashMap<String, String>>
}

/*impl Additional<&str> for String {
    fn update(&mut self, value: &str) {
        *self = value.to_string();
    }
}*/

#[derive(Serialize, Deserialize, Debug)]
struct DataOrigin {
    appid: Option<String>,
    journey_id: Option<String>,
    journey_version: Option<String>,
    data: Option<Value>,
    jwt: Option<JWTData>,
    token: Option<Token>,
    html: Option<String>,
    render_html: Option<Html>
}

fn advert_jouneys(data: &DataOrigin)  {
    let appid = data.appid.as_ref();
    let journey_id = data.journey_id.as_ref();
    let journey_version = &data.journey_version.as_ref();
    println!("{}", "¡¡¡¡REMEMBER!!!!".on_bright_red().bold());


    match appid{
        Some(value) => println!("{}{}", "Application: ".yellow(), &value.bright_yellow().bold().italic()),
        None => println!("{} no info", "Application: ".yellow())
    }

    match journey_id{
        Some(value) => println!("{}{}", "Journey id: ".green(), &value.bright_green().bold().italic()),
        None => println!("{} no info", "journey id: ".green())
    }

    match journey_version{
        Some(value) => println!("{}{}", "Journey version: ".blue(), &value.bright_blue().bold().italic()),
        None => println!("{} no info", "Journey version: ".blue())
    }
        //println!("¡¡¡¡REMEBER!!!!\r\n{}", result);


}

/*#[post("/check_numeric_pin", format = "json", data = "<msg>")]
fn check_numeric_pin(msg: Json<encrypt_request::PIN>) -> JsonValue {
    println!("{}", msg.pin.to_owned());
    let pin 
    json!(true)
}*/

#[post("/check_pin", format = "json", data = "<msg>")]
fn check_pin(msg: Json<encrypt_request::ManagePin>) -> JsonValue {
    let value = &msg.jwt;
    let jwt_pin = JWT::validate(value, encrypt_request::PUB_KEY_TRANSMIT.to_string());
    
    if jwt_pin.validation.unwrap().is_verified == Some(true) {
        let result = encrypt_request::check_pin(&msg.type_id, &msg.id, &msg.field,
            &jwt_pin.payload.as_ref().unwrap()["pin"].as_str().unwrap(), &msg.token, &msg.host);
        let value_pin = &result["result"][0][&msg.field].as_str();
        match value_pin {
            Some(v) => {
                let pin_str = format!("[{}]", v);
                let pin_vec: Vec<u8> = serde_json::from_str(&pin_str).unwrap();
                let pin = encrypt_request::decrypt_public(&pin_vec, encrypt_request::PUB_KEY_LDAP);
                let is_succesful = &pin == &jwt_pin.payload.unwrap()["pin"].as_str().unwrap();
                let message = if is_succesful == true {
                    "Correct Pin" } else {
                    "Incorrect Pin"
                };
                json!(
                    {"is_succesful": is_succesful,
                    "message": &message}
                )
            },
            None => json!({"message": "Pin not match", "is_succesful": false})
        }
    } else {
        json!({"message": "JWT not valid", "is_succesful": false})
    }
}

#[post("/check_numeric_pin", format = "json", data = "<msg>")]
fn check_numeric_pin(msg: Json<Token>) -> JsonValue {
    let value = &msg.value;
    let jwt_pin = JWT::validate(value, encrypt_request::PUB_KEY_TRANSMIT.to_string());
    if jwt_pin.validation.unwrap().is_verified == Some(true) {
            json!(true)
    } else {
        json!(false)
    }
}

#[post("/set_pin", format = "json", data = "<msg>")]
fn set_pin(msg: Json<encrypt_request::ManagePin>) -> JsonValue {
    let value = &msg.jwt;
    let jwt_pin = JWT::validate(value, encrypt_request::PUB_KEY_TRANSMIT.to_string());

    if jwt_pin.validation.unwrap().is_verified == Some(true) {
        let pin = &jwt_pin.payload.as_ref().unwrap()["pin"].as_str().unwrap().to_owned();
        let pin_encrypted = encrypt_request::encrypt_private(&pin, encrypt_request::PK_LDAP);
        let new_pin = &jwt_pin.payload.unwrap()["new_pin"].as_str().unwrap().to_owned();
        let new_pin_encrypted = encrypt_request::encrypt_private(&new_pin, encrypt_request::PK_LDAP);
        let result = encrypt_request::assign_pin(&msg.type_id, &msg.id, &msg.field, &pin_encrypted, &new_pin_encrypted, &msg.token, &msg.host);
        let message = if result == false {
            "Change of Pin wrong" } else {
            "Change of Pin correct wrong"
        };
        let is_succesful = if result == false {
            false
        } else {
            true
        };
        json!({"result": result, "message": message, "is_succesful": is_succesful})
    } else {
        json!({"message": "JWT not valid", "is_succesful": false})
    }
}

#[get("/generate_numeric_pin/<pin>")]
fn generate_numeric_pin(pin: i32) -> JsonValue {
    let pin_str = format!("{}", pin);
    json!(encrypt_request::encrypt_private(&pin_str, encrypt_request::PK_LDAP))

}

#[post("/decrypt_numeric_pin", format = "json", data = "<msg>")]
fn decrypt_numeric_pin(msg: Json<encrypt_request::PIN>) -> JsonValue {
    let pin = format!("[{}]", msg.pin.to_owned());
    let pin: Vec<u8> = serde_json::from_str(&pin).unwrap();
    println!("{:?}", &pin);
    json!(encrypt_request::decrypt(&pin, encrypt_request::PK_TRANSMIT))
}


#[post("/generate_private_certificate", format = "json", data = "<msg>")]
fn generate_certificate(msg: Json<X509Params>) -> JsonValue  {
    let version = msg.version.to_owned();
    let subject_name =  msg.subject_name.to_owned();
    let issuer_name = msg.issuer_name.to_owned();
    let not_before = msg.not_before;
    let not_after = msg.not_after;
    
    let cert = X509Params {
        version: version,
        subject_name: subject_name,
        issuer_name: issuer_name,
        not_before: not_before,
        not_after: not_after
    };

    let result = cert.generate_certificate(num);

    json!({"certificate": result})

}

#[get("/json/<uid>")]
fn get_json(uid: String, shared: State<SharedDataJson>) -> JsonValue {
    let shared_data: &SharedDataJson = shared.inner();
    let values = &shared_data.shared_data.lock().unwrap();
    let index = format!("data_{}", &uid);
    let response = values.get(&index);//.unwrap().to_owned();
    //if response.is_none() == false {
    //    json!(response.unwrap()) } else {
    //        json!({"error": "uid not found"})
    //    }
    match response {
        Some(value) => json!(value),
        None => json!({"error": "uid not found"})
    }
}


#[get("/view_html/<uid>")]
fn index(uid: String, shared: State<SharedData>) -> content::Html<String> {
    let shared_data: &SharedData = shared.inner();
    let pre_values = &shared_data.shared_data.lock().unwrap();
    let values = pre_values.get(&uid);
    let html_content = match values {
        Some(value) => value.to_owned(), 
        None => "NO INFO".to_string()
         };
    let response = content::Html(html_content);
    return response;
}

#[get("/html/<url>")]
fn index_html(url: String, shared: State<SharedDataHtmlRender>) -> content::Html<String> {
    let shared_data: &SharedDataHtmlRender = shared.inner();
    let pre_values = &shared_data.shared_data.lock().unwrap();
    let values = pre_values.get(&url);
    let html_content = match values {
       Some(value) => value.to_owned(), 
       None => "NO INFO".to_string()
        };
    let response = content::Html(html_content);
    return response;
}



#[post("/jwt", format = "json", data = "<msg>")]
fn validate_jwt(msg: Json<DataJWT>) -> Json<JWT>  {
    
    let jwt = &msg.jwt;
    let jwt = JWT::new(jwt.to_string());
    //println!("{:?}",&jwt.header);
    println!("Received: \r\n\r\n");
    let jwt_str = serde_json::to_string_pretty(&jwt);
    println!("{}",jwt_str.unwrap());
    return Json(jwt);
}

#[post("/show/<uid>/<id>", format = "json", data = "<msg>")]
fn generate_msg(msg: Json<DataOrigin>, uid:String, id: String, shared: State<SharedData>,
                shared_html: State<SharedDataHtmlRender>, shared_json: State<SharedDataJson>) -> JsonValue {

    println!("#######################################BEGIN##############################");
    let now: DateTime<Utc> = Utc::now();
    println!("TIME: {}", now.format("%T %e-%b-%Y"));
                    let url =
    if msg.render_html.is_none() == false {
        let render_html = msg.render_html.as_ref();
        let my_html = build_context(&render_html);
        let shared_data_html: &SharedDataHtmlRender = shared_html.inner();
        shared_data_html.shared_data.lock().unwrap().insert(render_html.unwrap().url.to_owned(), my_html.to_owned());
        (None, Some(format!("http://localhost/html/{}", render_html.unwrap().url)))
    } else {
        (Some("No data to render".to_string()),None)

    };
    
    let html = msg.html.as_ref();
    let shared_data: &SharedData = shared.inner();
    //let html_info = if html.is_none() == false {
    //    html.unwrap()
    //} else {
    //    "NO INFO"
    //};
    let html_info = match html {
        Some(value) => {
            println!("\r\n{} http://localhost:8000/view_html/{}", "url:".green(), &uid);
            value},
        None => "NO INFO"
    };
    shared_data.shared_data.lock().unwrap().insert(uid.to_string(), html_info.to_string());
    
    
    let status_code = show_data(&msg.data, &id, &uid);
    if status_code == 200 {
        shared_json.shared_data.lock().unwrap().insert(format!("data_{}", uid.to_owned()), msg.data.as_ref().unwrap().to_owned())}
    else {
        shared_json.shared_data.lock().unwrap().insert(format!("data_{}", uid.to_owned()), json_value!(null))
    };

    
    let jwt: JWT = create_jwt(&msg.token, &id);
    let token = create_token(&msg.jwt);
    
    println!("\r\nFrom user {} \r\nRESULTADO {:?}", &uid.blue().bold(), jwt);
    println!("For token {:?}\r\n\r\n", token);
    advert_jouneys(&msg);
    println!("#######################################END##############################");
    
    
    json!(
        {
            "url": {
                "value": url.1,
                "eror": url.0
            },
            "status_code": status_code,
            "jwt": jwt,
            "token": {
                "value": token.1,
                "error": token.0
            }
        }
    )
}

#[post("/generate_jwk", format = "json", data = "<msg>")]
fn generate_jwk(msg: Json<DataOriginJWK>, shared: State<SharedDataJWK>) -> JsonValue {
    
    let kid = &msg.kid;
    let public_key = &msg.public_key;
    let shared_data: &SharedDataJWK = shared.inner();
    shared_data.shared_data.lock().unwrap().insert(kid.to_owned(), public_key.to_owned());
    
    println!("Received {:?}", msg);
   json!(
       {
       "status": 200,
       "link": format!("http://localhost:8000/{}/jwks.json", kid)
       }
    ) 
}

#[get("/jwk/<kid>/jwks.json")]
fn get_jwk(kid: String, shared: State<SharedDataJWK>) -> Json<Keys>  {
    let shared_data: &SharedDataJWK = shared.inner();
    let values = &shared_data.shared_data.lock().unwrap();
    let mut jwkdata = JwkData::new();
    let public_key = &values.get(&kid);
    println!("El valor es {:?}", &values.get(&kid));
    jwkdata.generate_jwk(&kid, "example", public_key.unwrap());
    let jwkdata2 = jwkdata.clone();
    let jwkdata3 = jwkdata.clone();
    
    let result: Keys = Keys {
        key: vec!(jwkdata, jwkdata2, jwkdata3)
    };
    return Json(result);
}

//fn old_configure_rocket(port: u16) -> rocket::Config {
//    let config = Config::build(Environment::Staging)
//    .address("0.0.0.0")
//    .port(port)
//    .finalize().unwrap();
//
//    return config
//
//}//

//fn configure_rocket(port: u16) -> Figment {
//    let figment = rocket::Config::figment()
//    .merge(("address", "0.0.0.0"))
//    .merge(("port", port));

//    return figment
//}

fn main() {

    for port in 8000..9000 {
        //let config = configure_rocket(port);
        let config  = rocket::Config::figment()
            .merge(("address", "0.0.0.0"))
            .merge(("port", port));
        let app = rocket::custom(config);
        
        /*rocket::*/let error = app.mount("/", routes![generate_msg, validate_jwt, generate_jwk, get_jwk, index,
                                            index_html, get_json, generate_certificate, check_numeric_pin,
                                            generate_numeric_pin, decrypt_numeric_pin, check_pin, set_pin])
        .manage(SharedData{
            shared_data: Mutex::new(HashMap::new())
        })
        .manage(SharedDataJWK{
            shared_data: Mutex::new(HashMap::new())
        })
        .manage(SharedDataHtmlRender{
            shared_data: Mutex::new(HashMap::new())
        })
        .manage(SharedDataJson{
            shared_data: Mutex::new(HashMap::new())
        })
//        .attach(make_cors())
        .launch();

        println!("{:?}", error);
    }
}
